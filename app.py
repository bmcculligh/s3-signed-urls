import os
import uuid
import boto3
import json


def lambda_handler(event, context):
    try:
        unique_name = str(uuid.uuid4())
        region_name = os.environ["AWS_REGION"]
        # We HAVE to specify the region in the URL otherwise you get random timeouts from the S3 upload link
        client = boto3.client("s3", region_name=region_name, endpoint_url='https://s3.' + region_name + '.amazonaws.com')
        response = client.generate_presigned_url('put_object',
                                                 Params={'Bucket': os.environ["UPLOAD_BUCKET"],
                                                         'Key': unique_name,
                                                         'ContentType': os.environ['CONTENT_TYPE']},
                                                 ExpiresIn=os.environ['EXPIRY_TIME'])
        # print(json.dumps(response))
        return {
            'statusCode': 200,
            'headers': {},
            'body': json.dumps({"signedURL": response}),
        }
    except Exception as e:
        return {
            "statusCode": 400,
            "error": str(e)
        }
