# S3 Signed Urls
This application receives requests for a signed S3 URL which is capable of uploading a 5gb (default) file. The http request must be sent with an Auth0 JWT in the Authorization header.

The SAM template installs the following:
* S3 URL generator Lambda
* API Gateway API that talks to the Lambda
* Default Auth0 authorizer that works out of the box

## Getting Started
* Install Python 3.9
* Clone the project
* **Optional** Install PyCharm

---
## Deployment
* Run `sam build`
* Run `sam deploy --guided` filling in the relevant information
* Log into AWS and capture your stacks API Gateway URL

---
## Known Issues
* If you do not pass the bucket's region into the signed url, you intermittently will get timeouts. This appears to be caused by CORS but changing the buckets CORS policy to wide open didn't seem to fix this issue, whereas placing the regioin in the URL works every time.
* 

---
## Retrieving a JWT
* Generate an Auth0 token via a `POST` to the relevant API
* Copy the returned JWT
#### Example
`POST` URL: https://dev-vu6x7saa.us.auth0.com/oauth/token.

Required Headers: `application/json`

Body: `{
    "client_id":"WPZN8Wx3ZYjTz3gZnhyjImeNQjbm5oav","client_secret":"H5On4JOeMyumqzZjzXOwhG6s0YDjEAPzb0NeSy0XSrkmxoKVCbZfUY7pm4Hjtymk","audience":"https://bmcculligh-api-gateway","grant_type":"client_credentials"
}`

---
## Retrieving Signed URL
* Create a new `GET` request to retrieve a signed URL. This request will be against your stacks API Gateway URL. Copy this from AWS Console if you do not have it.
  * You **MUST** pass the Auth0 JWT in the `Authorization` header as `Bearer {TOKEN}`
#### Example
`GET` URL: https://51207mx2e1.execute-api.eu-west-1.amazonaws.com/dev/signed-url

Required Headers: `Authorization`: `Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6Im9tR1NNdFFVLS1wcjNGbUZ6OFBRdSJ9.eyJpc3MiOiJodHRwczovL2Rldi12dTZ4N3NhYS51cy5hdXRoMC5jb20vIiwic3ViIjoiV1BaTjhXeDNaWWpUejNnWm5oeWpJbWVOUWpibTVvYXZAY2xpZW50cyIsImF1ZCI6Imh0dHBzOi8vYm1jY3VsbGlnaC1hcGktZ2F0ZXdheSIsImlhdCI6MTY0Mzk4NTQxNywiZXhwIjoxNjQ0MDcxODE3LCJhenAiOiJXUFpOOFd4M1pZalR6M2dabmh5akltZU5RamJtNW9hdiIsImd0eSI6ImNsaWVudC1jcmVkZW50aWFscyJ9.Px7X8cetJktKnAO9j0t5UuQK2aBdLp6zE0E5PwLrNMaNYXv8avkmn0x3hJmZPXa2KkC2cNViT3zJXWvtMscFF7h5JxDMr8auvGflGEIKbgqkCyv0oF0nzvYBg0Ed23-hq0ElzOpXtNcJofJiHB3yLI6L3Di6E0N9sUeuYe6jGwOeP8mJy2ivUz552rcVK-RV-dMkhpxMHTkjCaylDHh4t-f0rwHxZ_rOztPtNHoBTXUET4q13Aafh7j0I9JPZk772KIlApmAiJEI38Rrz_rkV1bFG-oFd-la9Dr_A6GvcYgL6IR0P_zGHVvd1FxGvRHwYpEdYoOnH-1snigUjGIB0g`

---
## Uploading a File
* Crate a `PUT` request with the signed URL and the file you wish to upload in the body. 
  * You **MUST** pass the `Content-Type` header as `application/octet-stream`
* Confirm the file is in the configured S3 Bucket
#### Example
`PUT` URL: https://s3.eu-west-1.amazonaws.com/bm-python-upload-s3uploadbucket-fmte3guga5i2/81543523-c615-4250-bdf0-b74ce2c0511b?AWSAccessKeyId=ASIASHRWXA6SK7GNF6OS&Signature=gzLNrGYC15JJdwXqL%2F2rHQMWxoM%3D&content-type=application%2Foctet-stream&x-amz-security-token=IQoJb3JpZ2luX2VjEFsaCWV1LXdlc3QtMSJIMEYCIQDkGoqaVpUddq1Z5h11it1Eim1%2FrMIdtEHxKF4HQYMINgIhAIbDQaKbr38mlLCl5NgD5L%2BwRy9Kiz8qgv0xi0t0SdrLKrgCCJT%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEQARoMMTUzNjU5ODM2MzI0Igyd30i1KLxP54c3WGIqjALtvOE9VQ%2BhP4BcG%2BgMamputmfViRLqlLTwFTJouP1BarFhypX9zdMANRoDBPzfeUssTb3xQ3BXXV5BIOIMacs98MtT%2FUG6Ii6RIhHCveYyr6%2BMJ%2FN6RGK6hMe4J5d8ROsweq4QdJEd2iNUMN8xUWttTQuWQo%2BWNkj%2F40C6bMsgY9oil%2BcTDl8%2BnLGF%2FpDgCasPSKCQgOoG31JO2ke6sAZnFpxL1WJ88ScLVvw1yNb2EqFiJygS3LYLE%2BMrF4npj%2F%2FEM20LrRZpkX9yPwm3t7Biu%2Fd1jAm1yOrVRvT1tI%2FJAwsXpksvn9sc1fTtIPMQ66xbdkZzalGS9UFS9PsD%2FrvWGYhJBIX8Sjri5R1IMPrv9Y8GOpkByBZ9D8FjnydGb%2FT5jdalze9HtOurbx%2FdcAsiIP3jzUC0N%2B9aFEu71xXIdT8jMyv%2FiAsphk49YBPMCLrjxbZwRlDy7Amo9S%2BM9Jsy0IMlUHbsHSbJjhxkWoowsI2xe%2Fl5YmIZ9GYV6E31A%2F8TRJ9TQK%2BRfpfmz9mTJgzOOwtIrahssuPeEcIxVEbAnVMcwJzU7HB9es%2BFooI2&Expires=1644001678

Required Headers: `Content-Type`: `application/octet-stream`

Body: file-to-upload

----